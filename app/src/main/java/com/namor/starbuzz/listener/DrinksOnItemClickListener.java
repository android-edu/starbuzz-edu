package com.namor.starbuzz.listener;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.widget.AdapterView;

import com.namor.starbuzz.DrinkActivity;

public class DrinksOnItemClickListener implements AdapterView.OnItemClickListener {
    private Context context;

    public DrinksOnItemClickListener(Context context) {
        this.context = context;
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long id) {
        Intent intent = new Intent(context, DrinkActivity.class);
        intent.putExtra(DrinkActivity.DRINK_ID, (int) id);

        context.startActivity(intent);
    }
}
