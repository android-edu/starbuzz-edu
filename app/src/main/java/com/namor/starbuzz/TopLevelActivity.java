package com.namor.starbuzz;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CursorAdapter;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;

import com.namor.starbuzz.listener.DrinksOnItemClickListener;

public class TopLevelActivity extends Activity {

    private SQLiteDatabase db;
    private Cursor cursor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_top_level);

        initOptionsListView();
        initFavoriteListView();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        cursor.close();
        db.close();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        updateFavoriteListView();
    }

    private void updateFavoriteListView() {
        Cursor newCursor = getFavoriteDrinksCursor();
        ListView favoriteList = findViewById(R.id.list_favorite);
        CursorAdapter adapter = (CursorAdapter) favoriteList.getAdapter();
        adapter.changeCursor(newCursor);
        cursor = newCursor;
    }

    private void initOptionsListView() {
        AdapterView.OnItemClickListener listener = new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                if (i == 0) {
                    Intent intent = new Intent(TopLevelActivity.this,
                            DrinkCategoryActivity.class);

                    startActivity(intent);
                }
            }
        };

        ListView listView = (ListView) findViewById(R.id.list_options);
        listView.setOnItemClickListener(listener);
    }

    private void initFavoriteListView() {
        SQLiteOpenHelper dbHelper = new StarbuzzDatabaseHelper(this);
        db = dbHelper.getReadableDatabase();
        cursor = getFavoriteDrinksCursor();

        ListView favorite = findViewById(R.id.list_favorite);
        favorite.setAdapter(new SimpleCursorAdapter(
                this,
                android.R.layout.simple_list_item_1,
                cursor,
                new String[] {"NAME"},
                new int[] {android.R.id.text1},
                0
        ));

        favorite.setOnItemClickListener(new DrinksOnItemClickListener(this));
    }

    private Cursor getFavoriteDrinksCursor() {
        return db.query(
                "DRINK",
                new String[] {"_id", "NAME"},
                "FAVORITE = ?",
                new String[] {Integer.toString(1)},
                null, null, null);
    }
}
