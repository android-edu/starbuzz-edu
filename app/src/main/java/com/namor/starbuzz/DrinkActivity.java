package com.namor.starbuzz;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class DrinkActivity extends Activity {
    final public static String DRINK_ID = "DrinkID";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_drink);

        Intent sourceIntent = getIntent();
        int drinkId = sourceIntent.getIntExtra(DRINK_ID, 0);
        loadDrink(drinkId);
    }

    public void onFavoriteClicked(View view) {
        int drinkId = getIntent().getIntExtra(DRINK_ID, 0);
        new UpdateDrinkTask().execute(drinkId);
    }

    private void loadDrink(int drinkID) {
        SQLiteOpenHelper dbHelper = new StarbuzzDatabaseHelper(this);
        try (
                SQLiteDatabase db = dbHelper.getReadableDatabase();
                Cursor cursor = db.query("DRINK",
                        new String[] {"NAME", "DESCRIPTION", "IMAGE_RESOURCE_ID", "FAVORITE"},
                        "_id = ?",
                        new String[] {Integer.toString(drinkID)},
                        null, null, null)
        ) {
            if (cursor.moveToFirst()) {
                String name = cursor.getString(0);
                String description = cursor.getString(1);
                int imageResourceId = cursor.getInt(2);
                int check = cursor.getInt(3);

                ((TextView) findViewById(R.id.headerTextView)).setText(name);
                ((TextView) findViewById(R.id.descriptionTextView)).setText(description);
                ((ImageView) findViewById(R.id.imageView)).setImageResource(imageResourceId);
                ((CheckBox) findViewById(R.id.favorite)).setChecked(check != 0);
            }
        } catch (SQLiteException e) {
            Toast.makeText(this, "Database unavailable", Toast.LENGTH_SHORT).show();
        }
    }

    private class UpdateDrinkTask extends AsyncTask<Integer, Void, Boolean> {
        private ContentValues drinkValues;

        @Override
        protected void onPreExecute() {
            CheckBox favorite = findViewById(R.id.favorite);
            drinkValues = new ContentValues();
            drinkValues.put("FAVORITE", favorite.isChecked());
        }

        @Override
        protected Boolean doInBackground(Integer... drinks) {
            int drinkId = drinks[0];

            SQLiteOpenHelper dbHelper = new StarbuzzDatabaseHelper(DrinkActivity.this);
            try (SQLiteDatabase db = dbHelper.getWritableDatabase()) {
                db.update("DRINK",
                        drinkValues,
                        "_id = ?",
                        new String[]{Integer.toString(drinkId)});

                return true;
            } catch (SQLException e) {
                return false;
            }
        }

        @Override
        protected void onPostExecute(Boolean result) {
            if (!result) {
                Toast.makeText(DrinkActivity.this,
                        "Database unavailable", Toast.LENGTH_SHORT).show();
            }
        }
    }
}
