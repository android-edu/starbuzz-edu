package com.namor.starbuzz;

import android.app.Activity;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Bundle;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;

import com.namor.starbuzz.listener.DrinksOnItemClickListener;

public class DrinkCategoryActivity extends Activity {

    private SQLiteDatabase db;
    private Cursor cursor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_drink_category);

        ListView listView = findViewById(R.id.drink_list);

        SimpleCursorAdapter adapter = getCursorAdapter();
        listView.setAdapter(adapter);

        AdapterView.OnItemClickListener listener = new DrinksOnItemClickListener(this);
        listView.setOnItemClickListener(listener);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        cursor.close();
        db.close();
    }

    private SimpleCursorAdapter getCursorAdapter() {
        SQLiteOpenHelper helper = new StarbuzzDatabaseHelper(this);
        db = helper.getReadableDatabase();
        cursor = db.query(
                "DRINK",
                new String[] {"_id", "NAME"},
                null, null, null, null, null);

        return new SimpleCursorAdapter(
                this,
                android.R.layout.simple_list_item_1,
                cursor,
                new String[] {"NAME"},
                new int[] {android.R.id.text1},
                0
        );
    }
}
